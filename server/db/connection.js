const admin = require("firebase-admin");

const serviceAccount = process.env.FIRESTORE_KEY_PATH;

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

module.exports = { db };
