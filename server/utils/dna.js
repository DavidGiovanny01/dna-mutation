const isValid = (dna) => {
  // Validating DNA
  if (!Array.isArray(dna)) return false;
  const allDNA = dna.length;
  if (allDNA < 4) return false;
  for (let i = 0; i < allDNA; i++) {
    if (typeof dna[i] !== "string" || dna[i].length !== allDNA) return false;
    for (let j = 0; j < allDNA; j++)
      if (!"ATCG".includes(dna[i][j])) return false;
  }
  return true;
};

const hasMutation = (dna) => {
  const allDNA = dna.length;
  // Iterations to find the mutation
  let prevV = [];
  for (let i = 0; i < allDNA; i++) {
    let prevH = "";
    for (let j = 0, baseDNA; j < allDNA; j++) {
      baseDNA = dna[i][j];
      // Looking for horizontal coincidences
      if (j > 0 && baseDNA === dna[i][j - 1]) {
        if (prevH.length === 3) return true;
        prevH += baseDNA;
      } else {
        prevH = baseDNA;
      }

      // Looking for vertical coincidences
      if (i > 0 && baseDNA === dna[i - 1][j]) {
        if (prevV[j].length === 3) return true;
        prevV[j] += baseDNA;
      } else {
        prevV[j] = baseDNA;
      }

      // Looking for oblique (\) coincidences
      if (allDNA - i > 3 && allDNA - j > 3) {
        for (let k = 0; k < 4; k++) {
          if (baseDNA !== dna[i + k][j + k]) break;
          if (k === 3) return true;
        }
      }

      // Looking for oblique (/) coincidences
      if (i > 2 && allDNA - j > 3) {
        for (let k = 0; k < 4; k++) {
          if (baseDNA !== dna[i - k][j + k]) break;
          if (k === 3) return true;
        }
      }
    }
  }
  return false;
};

module.exports = { isValid, hasMutation };
