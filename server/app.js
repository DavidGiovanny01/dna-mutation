require("./config/config");
const { db } = require("./db/connection");
const express = require("express");
const app = express();
const bodyParser = require("body-parser");
const { body, validationResult } = require("express-validator");

const dna = require("./utils/dna");

app.use(bodyParser.json());

app.use((err, _req, res, _next) => {
  err instanceof SyntaxError &&
    err.status == 400 &&
    "body" in err &&
    res.status(400).json({ message: "Invalid JSON" });
});

const validateDNA = [
  body("dna", "DNA is required").exists(),
  body("dna", "DNA needs to be an array").isArray(),
];

app.post("/mutation", validateDNA, async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(401).json({ errors: errors.array() });
  }
  const body = req.body;
  if (!dna.isValid(body.dna)) {
    return res.status(400).send();
  }

  const dnaSequencesRef = db.collection("dna-sequences");
  const dnaSequences = await dnaSequencesRef.where("dna", "==", body.dna).get();

  let mutation;
  if (dnaSequences.empty) {
    mutation = dna.hasMutation(body.dna);
    dnaSequencesRef.add({
      dna: body.dna,
      mutation,
    });

    const dnaStats = await dnaSequencesRef.doc("stats").get();
    if (dnaStats.exists) {
      let stats = dnaStats.data();
      stats.count_mutations += mutation ? 1 : 0;
      stats.count_no_mutations += mutation ? 0 : 1;
      stats.ratio = stats.count_mutations / stats.count_no_mutations;
      if (stats.ratio === Infinity) {
        stats.ratio = stats.count_mutations;
      }
      await dnaSequencesRef.doc("stats").update(stats);
    } else {
      await dnaSequencesRef.doc("stats").set({
        count_mutations: mutation ? 1 : 0,
        count_no_mutations: mutation ? 0 : 1,
        ratio: mutation ? 1 : 0,
      });
    }
  } else {
    dnaSequences.forEach((doc) => {
      mutation = doc.data().mutation;
    });
  }

  res.statusCode = mutation ? 200 : 403;
  res.send();
});

app.get("/stats", async (_req, res) => {
  const dnaSequencesRef = db.collection("dna-sequences");
  const dnaStats = await dnaSequencesRef.doc("stats").get();
  let stats;
  if (dnaStats.exists) {
    stats = dnaStats.data();
  } else {
    stats = {
      count_mutations: 0,
      count_no_mutations: 0,
      ratio: 0,
    };
  }
  res.send({ stats });
});

module.exports = { app };
