const { db } = require("../server/db/connection");

const dnaUnmutated = [
  "ATGCGA",
  "CAGTGC",
  "TTATTT",
  "AGACGG",
  "GCGTCA",
  "TCACTG",
];

const dnaMutated = ["ATGCGA", "CAGTGC", "TTATGT", "AGAAGG", "CCCCTA", "TCACTG"];

const dnaShort = ["ATG", "CAG", "TTA"];

const dnaInvalidType = ["CAGG", 1234, "TTAA", "TTAA"];

const dnaInvalidLength = ["CAGG", "CAGT", "TTAAA", "AGAG"];

const dnaInvalidBase = ["CAGG", "CAGT", "STAA", "AGAG"];

const dnaMutatedH = ["ATGCG", "CAGTG", "TTTTG", "AGAAG", "CAGTG"];

const dnaMutatedV = ["ATGCG", "CAGTG", "TATTG", "AAGAG", "CAGTG"];

const dnaMutatedO1 = ["ATGCG", "CAGGG", "TAATG", "AGGAG", "CAGTG"];

const dnaMutatedO2 = ["ATGCG", "CAGGG", "TAGTG", "AGGAG", "CAGTG"];

const initialDNASequences = [
  {
    dna: ["ATGC", "CTGT", "TTAT", "AGAA"],
    mutation: false,
  },
  {
    dna: ["ATGC", "CAGT", "TTAT", "AGAA"],
    mutation: true,
  },
];

const populateDNASequences = async () => {
  const dnaSequencesRef = db.collection("dna-sequences");
  await deleteCollection(db, "dna-sequences");

  await dnaSequencesRef.doc().set(initialDNASequences[0]);
  await dnaSequencesRef.doc().set(initialDNASequences[1]);
  await dnaSequencesRef.doc("stats").set({
    count_mutations: 1,
    count_no_mutations: 1,
    ratio: 1,
  });
};

const populateDNASequence = async () => {
  const dnaSequencesRef = db.collection("dna-sequences");
  await deleteCollection(db, "dna-sequences");

  await dnaSequencesRef.doc().set(initialDNASequences[1]);
  await dnaSequencesRef.doc("stats").set({
    count_mutations: 1,
    count_no_mutations: 0,
    ratio: 0,
  });
};

const clearDNASequences = async () => {
  await deleteCollection(db, "dna-sequences");
};

function deleteCollection(db, collectionPath) {
  var collectionRef = db.collection(collectionPath);

  return new Promise((resolve, reject) => {
    deleteQueryBatch(db, collectionRef, resolve, reject);
  });
}

function deleteQueryBatch(db, query, resolve, reject) {
  query
    .get()
    .then((snapshot) => {
      if (snapshot.size == 0) return 0;

      const batch = db.batch();
      snapshot.docs.forEach((doc) => {
        batch.delete(doc.ref);
      });

      return batch.commit().then(() => {
        return snapshot.size;
      });
    })
    .then((numDeleted) => {
      if (numDeleted === 0) {
        resolve();
        return;
      }

      process.nextTick(() => {
        deleteQueryBatch(db, query, resolve, reject);
      });
    })
    .catch(reject);
}

module.exports = {
  dnaUnmutated,
  dnaMutated,
  dnaShort,
  dnaInvalidType,
  dnaInvalidLength,
  dnaInvalidBase,
  dnaMutatedH,
  dnaMutatedV,
  dnaMutatedO1,
  dnaMutatedO2,
  populateDNASequences,
  populateDNASequence,
  clearDNASequences,
  initialDNASequences,
};
