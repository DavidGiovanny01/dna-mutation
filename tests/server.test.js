const request = require("supertest");

const { app } = require("../server/app");
const {
  dnaUnmutated,
  dnaMutated,
  dnaShort,
  dnaInvalidType,
  dnaInvalidLength,
  dnaInvalidBase,
  dnaMutatedH,
  dnaMutatedV,
  dnaMutatedO1,
  dnaMutatedO2,
  populateDNASequences,
  populateDNASequence,
  clearDNASequences,
  initialDNASequences,
} = require("./seed");
const dna = require("../server/utils/dna");

describe("POST /mutation", () => {
  beforeEach(populateDNASequences);

  test("should response with 403 if not exist a mutation", async () => {
    await request(app)
      .post("/mutation")
      .send({ dna: dnaUnmutated })
      .type("json")
      .expect(403);
  });

  test("should response with 200 if exist a mutation", async () => {
    await request(app)
      .post("/mutation")
      .send({ dna: dnaMutated })
      .type("json")
      .expect(200);
  });

  test("should response with 403 for unmutated dna if dna sequence already is stored", async () => {
    await request(app)
      .post("/mutation")
      .send(initialDNASequences[0])
      .type("json")
      .expect(403);
  });

  test("should response with 400 & message for invalid JSON if the JSON request is wrong", async () => {
    const response = await request(app)
      .post("/mutation")
      .send('{"invalid"}')
      .type("json")
      .expect("Content-Type", /json/)
      .expect(400);
    expect(response.body).toMatchObject({ message: "Invalid JSON" });
  });

  test("should response with 401 & array of error objects if the request not contain the dna array", async () => {
    const response = await request(app)
      .post("/mutation")
      .send('{"valid": true}')
      .type("json")
      .expect("Content-Type", /json/)
      .expect(401);
    expect(response.body).toHaveProperty("errors");
    expect(response.body.errors).toHaveLength(2);
  });

  test("should response with 400 if is invalid dna input", async () => {
    await request(app).post("/mutation").send({ dna: dnaShort }).expect(400);
  });
});

describe("POST /mutation", () => {
  beforeEach(clearDNASequences);

  test("should response with 200 and create the first stats document", async () => {
    await request(app).post("/mutation").send({ dna: dnaMutated }).expect(200);
  });

  test("should response with 403 and create the first stats document", async () => {
    await request(app)
      .post("/mutation")
      .send({ dna: dnaUnmutated })
      .expect(403);
  });
});

describe("POST /mutation", () => {
  beforeEach(populateDNASequence);

  test("should response with 200 and check the calculation of the radius.", async () => {
    await request(app).post("/mutation").send({ dna: dnaMutated }).expect(200);
  });
});

describe("GET /stats", () => {
  beforeEach(populateDNASequences);

  test("should response with 200 and JSON with stats field if exists stored data", async () => {
    let response = await request(app).get("/stats").type("json").expect(200);
    expect(response.body).toHaveProperty("stats");
    expect(response.body.stats).toMatchObject({
      count_mutations: 1,
      count_no_mutations: 1,
      ratio: 1,
    });
  });
});

describe("GET /stats", () => {
  beforeEach(clearDNASequences);

  test("should response with 200 and default JSON if not exists stored stats", async () => {
    let response = await request(app).get("/stats").type("json").expect(200);
    expect(response.body).toHaveProperty("stats");
    expect(response.body.stats).toMatchObject({
      count_mutations: 0,
      count_no_mutations: 0,
      ratio: 0,
    });
  });
});

describe("DNA isValid function", () => {
  it("should return false if is not an array", () => {
    expect(dna.isValid("ATGCGA")).toBeFalsy();
  });

  it("should return false if array is too small", () => {
    expect(dna.isValid(dnaShort)).toBeFalsy();
  });

  it("should return false if is not an array of strings", () => {
    expect(dna.isValid(dnaInvalidType)).toBeFalsy();
  });

  it("should return false if is not a matrix of NxN", () => {
    expect(dna.isValid(dnaInvalidLength)).toBeFalsy();
  });

  it("should return false if the DNA base is wrong", () => {
    expect(dna.isValid(dnaInvalidBase)).toBeFalsy();
  });
});

describe("DNA hasMutation function", () => {
  it("should return true if find a horizontal mutation", () => {
    expect(dna.hasMutation(dnaMutatedH)).toBeTruthy();
  });

  it("should return true if find a vertical mutation", () => {
    expect(dna.hasMutation(dnaMutatedV)).toBeTruthy();
  });

  it("should return true if find an oblique (\\) mutation", () => {
    expect(dna.hasMutation(dnaMutatedO1)).toBeTruthy();
  });

  it("should return true if find an oblique (/) mutation", () => {
    expect(dna.hasMutation(dnaMutatedO2)).toBeTruthy();
  });

  it("should return false if dna has not a mutation", () => {
    expect(dna.hasMutation(dnaUnmutated)).toBeFalsy();
  });
});
